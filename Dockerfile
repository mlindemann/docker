FROM ubuntu:xenial

RUN apt-get update && \
	apt-get install -y --no-install-recommends python3 curl ca-certificates && \
    apt-get clean all

RUN ln -s /usr/bin/python3 /usr/bin/python

RUN mkdir /home/dev && \
    useradd dev -d /home/dev && \
    curl -L https://bitbucket.org/mlindemann/docker/get/master.tar.gz > /home/dev/master.tar.gz && \
    chown -R dev:dev /home/dev && \
    cd /home/dev && \
    tar xvzf master.tar.gz && \
    rm -f master.tar.gz && \
	mv /home/dev/mlindemann* /home/dev/src

USER dev

VOLUME /home/dev

WORKDIR /home/dev/src/mlindemann/docker/master

ENTRYPOINT python App.py